package testing;

import ForTest.Search;
import ForTest.ForThirdTest;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import io.qameta.allure.junit4.DisplayName;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.Augmenter;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import static org.junit.Assert.assertEquals;

public class ThirdTest extends WebDriverSettings {

    @Attachment(value = "{name}", type = "image/png")
    public static byte[] getFullScreenshot(WebDriver driver, String name) throws IOException {
        WebDriver augmented = new Augmenter().augment(driver);
        Screenshot shot = new AShot().takeScreenshot(augmented);
        BufferedImage img = shot.getImage();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(img, "png", baos);
        baos.flush();
        byte[] imageInByte = baos.toByteArray();
        baos.close();
        return imageInByte;
    }

    @Rule
    public TestWatcher screenshotOnFailure = new TestWatcher() {
        @Override
        protected void failed(Throwable e, Description description) {
            try {
                getFullScreenshot(driver,"Скриншот: ошибка");
            } catch (IOException f) { }
        }
    };

    @Step("Заходим на сайт Ozon")
    public static void IntoOzon(WebDriver driver)throws IOException
    {
        driver.get("https://www.ozon.ru/");
        assertEquals("OZON — интернет-магазин. Миллионы товаров по выгодным ценам",driver.getTitle());

        getFullScreenshot(driver, "скрин: Главная страница");
    }

    /*@Step("Скриншот главной страници")
    public static void ScreenHomePage  (WebDriver driver) throws IOException
    {
        getFullScreenshot(driver, "Скрнишот главной страници");
    }*/



    /*@Step("перейти на страницу c регистрацией")
    public static void check_in(WebDriver driver)throws IOException
    {
        WebElement myElement = driver.findElement(By.xpath("//*[@id=\"__layout\"]/div[1]/div[1]/header[1]/div[2]/div[3]/div[1]/div[1]"));
        Actions builder = new Actions(driver);
        builder.moveToElement(myElement).build().perform();
        driver.findElement(By.xpath("//*[@id=\"__layout\"]/div[1]/div[1]/header[1]/div[2]/div[3]/div[1]/div[1]/div[2]/div[2]/span[1] ")).click();
    }

    @Step("Создать аккаунта как юрлицо")
    public static void entity(WebDriver driver)throws IOException
    {
        ForThirdTest entity = new ForThirdTest(driver);
        entity.entity();
    }

    @Step("ввод имени и фамилии")
    public static void FI(WebDriver driver)throws IOException
    {
        ForThirdTest FI = new ForThirdTest(driver);
        FI.FI("Иван Иванович");
    }
    @Step("Ввод некоректной почты")
    public static void email(WebDriver driver)throws IOException
    {
        ForThirdTest email = new ForThirdTest(driver);
        email.email("htrhtrh");
    }*/

    @Step("Ввод некорректных символов")
    public static void InvalidCharacters (WebDriver driver)throws IOException
    {
        Search InvalidCharacters = new Search(driver);
        InvalidCharacters.Search("ase6+21e");

        JavascriptExecutor jse = ((JavascriptExecutor) driver);
        getFullScreenshot(driver, "Скриншот страницы после ввод некорректных символов");
    }

    /*@Step("Скриншот страницы после ввод некорректных символов")
    public static void ScreenInvalidCharacters  (WebDriver driver) throws IOException
    {
        JavascriptExecutor jse = ((JavascriptExecutor) driver);
        getFullScreenshot(driver, "Скриншот страницы после ввод некорректных символов");
    }*/

    @Step("ввод коректных символов")
    public static void Сorrectly  (WebDriver driver)throws IOException
    {
        Search correctly = new Search(driver);
        correctly.Search("Телефон ALCATEL T56 white");

        getFullScreenshot(driver, "Скриншот страницы после ввод коректных символов");
    }

    /*@Step("Скриншот страницы после ввод коректных символов")
    public static void ScreenСorrectly  (WebDriver driver) throws IOException
    {
        getFullScreenshot(driver, "Скриншот страницы после ввод коректных символов");
    }*/

    @Step("переход по товару 'ALCATEL T56 white'")
    public static void Transition(WebDriver driver)throws IOException
    {
        JavascriptExecutor jse = ((JavascriptExecutor) driver);
        jse.executeScript("scroll(0, 250);");
        ForThirdTest transition = new ForThirdTest(driver);
        transition.transition();

        getFullScreenshot(driver, "Скриншот страницы с товаром 'ALCATEL T56 white'");
    }

    /*@Step("Скриншот страницы с товаром 'ALCATEL T56 white'")
    public static void ScreenTransition  (WebDriver driver) throws IOException
    {
        JavascriptExecutor jse = ((JavascriptExecutor) driver);
        jse.executeScript("scroll(0, 250);");
        getFullScreenshot(driver, "Скриншот страницы с товаром 'ALCATEL T56 white'");
    }*/

    @Step("добавить в избранное")
    public static void Favorites(WebDriver driver)throws IOException
    {
        ForThirdTest favorites = new ForThirdTest(driver);
        favorites.favorites();

        getFullScreenshot(driver, "Скриншот добавить в избранное");
    }

    @Step("переход 'в избранное'")
    public static void ToFavorites(WebDriver driver)throws IOException
    {
        ForThirdTest toFavorites = new ForThirdTest(driver);
        toFavorites.toFavorites();

        JavascriptExecutor jse = ((JavascriptExecutor) driver);
        jse.executeScript("scroll(0, 300);");
        getFullScreenshot(driver, "Скриншот страницы избранное");
    }

    /*@Step("Скриншот страницы избранное")
    public static void ScreenToFavorites  (WebDriver driver) throws IOException
    {
        JavascriptExecutor jse = ((JavascriptExecutor) driver);
        jse.executeScript("scroll(0, 300);");
        getFullScreenshot(driver, "Скриншот страницы избранное");
    }*/

    @Step("Выход из браузера")
    public static void WaitAndQuit(WebDriver driver) throws InterruptedException
    {
        Thread.sleep(1000);
        driver.close();
        driver.quit();
        System.out.println("test close");
    }

    @Test
    @DisplayName("тест для проверки: ввода некорректных символов,ввод коректных символов, добавление в избранное")
    public void Test3() throws InterruptedException,IOException
    {
        IntoOzon(driver);
        //ScreenHomePage(driver);
        InvalidCharacters(driver);
        //ScreenInvalidCharacters(driver);
        Сorrectly(driver);
        //ScreenСorrectly(driver);
        Transition(driver);
        //ScreenTransition(driver);
        Favorites(driver);
        ToFavorites(driver);
        //ScreenToFavorites(driver);
        WaitAndQuit(driver);
    }
}