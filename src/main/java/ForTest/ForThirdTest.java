package ForTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ForThirdTest {
    private static String OZON_URL = "https://www.ozon.ru";

    WebDriverWait waitFor;

    @FindBy(xpath = "//*[@id=\"name\"]")
    private WebElement transition;

    @FindBy(xpath = "//*[@id=\"__layout\"]/div[1]/div[1]/div[4]/div[1]/div[2]/div[3]/div[1]/div[2]/div[1]/button[1]")
    private WebElement favorites;

    @FindBy(xpath = "//*[@id=\"__layout\"]/div[1]/div[1]/header[1]/div[2]/div[3]/div[3]/a[1]/span[1]")
    private WebElement toFavorites;

    /*@FindBy(xpath = "//*[@id=\"__layout\"]/div[1]/div[1]/header[1]/div[2]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/form[1]/div[1]/div[3]/a[2]")
    private WebElement entity;*/

    /*@FindBy(xpath = "//*[@id=\"__layout\"]/div[1]/div[1]/header[1]/div[2]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/form[1]/div[1]/div[2]/input[1]")
    private WebElement FI;

    @FindBy(xpath = "//*[@id=\"__layout\"]/div[1]/div[1]/header[1]/div[2]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/form[1]/div[1]/div[3]")
    private WebElement email;

    @FindBy(xpath = "//*[@id=\"__layout\"]/div[1]/div[1]/header[1]/div[2]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/form[1]/div[1]/div[4]")
    private WebElement password;*/

    public ForThirdTest(WebDriver driver) {
        if (!driver.getCurrentUrl().contains(OZON_URL)) {
            throw new IllegalStateException("This is not the page you are expected");
        }
        PageFactory.initElements(driver, this);
        this.waitFor = new WebDriverWait(driver, 3, 300);
    }

    public void transition(){transition.click();}
    public void favorites(){favorites.click();}
    public void toFavorites(){toFavorites.click();}


    //public void entity(){entity.click();}
   /* public void FI(String item){
        waitFor.until(ExpectedConditions.visibilityOf(FI));
        FI.clear();
        FI.sendKeys(item);
        FI.sendKeys(Keys.TAB);
    }*/

   /* public void email(String item) {
        email.clear();
        email.sendKeys(item);
    }

    public void password(String item) {
        password.click();
        password.sendKeys(item);
    }*/
}
